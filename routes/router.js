const express = require("express");
var bodyParser = require("body-parser");
const path = require("path");
const router = express.Router();
const fs = require("fs");

// create application/json parser
var jsonParser = bodyParser.json();

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

router.get("/", (_, res) => {
  // console.log(_.cookies)

  res.sendFile(path.join(__dirname, "../views", "welcome.html"));
});

router.get("/chat", (_, res) => {
  res.sendFile(path.join(__dirname, "../views", "chat.html"));
});

router.get("/new-chat", (_, res) => {
  res.sendFile(path.join(__dirname, "../views", "ended.html"));
});
router.get("/ads.txt", (_, res) => {
  res.sendFile(path.join(__dirname, "../views", "ads.txt"));
});
router.get("/getcountry", (_, res) => {
  var geoip = require("geoip-lite");

  var ip =
    _.headers["x-forwarded-for"] != undefined
      ? _.headers["x-forwarded-for"].split(",")[0].split(",")[0]
      : "178.165.22.132";
  var geo = geoip.lookup(ip);

  res.send(geo != null ? geo.country : "US");
});

router.use((_, res) => {
  res.status(404).sendFile(path.join(__dirname, "../views", "404.html"));
});

module.exports = router;
