/* eslint-disable indent */
new EmojiPicker();
/* Define connection status constants */
const NOT_CONNECTED = 1;
const CONNECTION_PENDING = 2;
const NO_USERNAME = 3;
const USERNAME_PENDING = 4;
const ROOM_PENDING = 5;
const CHAT_ON = 6;
// (Some of them may be useless)

let status = NOT_CONNECTED;

let username;

// Functions to work with cookies
function getCookie(name) {
  let matches = document.cookie.match(
    new RegExp(
      "(?:^|; )" +
        name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") +
        "=([^;]*)"
    )
  );
  return matches ? decodeURIComponent(matches[1]) : undefined;
}
function setCookie(name, value, options = {}) {
  options = {
    path: "/",
    ...options,
  };

  if (options.expires instanceof Date) {
    options.expires = options.expires.toUTCString();
  }

  let updatedCookie =
    encodeURIComponent(name) + "=" + encodeURIComponent(value);

  for (let optionKey in options) {
    updatedCookie += "; " + optionKey;
    let optionValue = options[optionKey];
    if (optionValue !== true) {
      updatedCookie += "=" + optionValue;
    }
  }

  document.cookie = updatedCookie;
}
function deleteCookie(name) {
  setCookie(name, "", {
    "max-age": -1,
  });
}

/* Initiate connection */
function connect() {
  /* If a connection attempt has already been emitted, don't try again... */
  if (status === CONNECTION_PENDING) return 1;
  /* ...else, launch socket.io */ else {
    status = CONNECTION_PENDING;
    var socket = io();
  }
  /* Bind socket to document because fuck you that's the only thing working */
  document.socket = socket;
  /* Start timer for connection timeout error */
  let connection_error_pending = setTimeout(connectionError, 3000);
  /* Wait for connection succes confirmation... */
  socket.on("connection success", () => {
    /* Remove EL in case of Enter key not used to connect */
    document.removeEventListener("keydown", enterKeyConnectAttempt);
    /* Stop timer, change status */
    clearTimeout(connection_error_pending);
    status = NO_USERNAME;
    // let message =
    //   "<font size=\"3\"><span uk-icon='icon: check; ratio: 1.5'></span>  Vous êtes connecté !</font>";
    // UIkit.notification({
    //   message: message,
    //   pos: "bottom-center",
    //   status: "success",
    // });
    /* Reveal username form page */
    // welcomPageOverlayOff();
    /* Enter key triggers a username proposal */
    document.addEventListener("keydown", enterKeyUsernameProp);
    /* Wait for username confirmation... */
    socket.on("accepted username", (usrnm) => {
      /* Wait for an available room */
      status = ROOM_PENDING;
      /* Cache username */
      username = usrnm;
      /* Initiate chat logic */
      initChat(socket, username);
      //   let message =
      //     "<font size=\"3\"><span uk-icon='icon: check; ratio: 1.5'></span>  Bienvenue </font>" +
      //     username +
      //     '<font size="3"> !</font>';
      //   UIkit.notification({
      //     message: message,
      //     pos: "bottom-center",
      //     status: "success",
      //   });
      /* Reveal spinner page */
      //   usernameFormPageOverlayOff();
      /* Wait for room confirmation */
      socket.on("joined room", (data) => {
        /* Reveal chat page */
        // setTimeout(spinnerPageOverlayOff, 700);
        // let message =
        //   "<font size=\"3\"><span uk-icon='icon: users; ratio: 1.5'></span>  Vous êtes dans le salon n°</font>" +
        //   data.room +
        //   '<font size="3"> !</font>';
        // UIkit.notification({
        //   message: message,
        //   pos: "bottom-center",
        //   status: "success",
        // });
        /* Go chatting! */
        status = CHAT_ON;
        /* Respond to "Enter" key press */
        document.addEventListener("keydown", enterKeySendsMsg);
        console.log(data);
        /* Update interlocutor */
        updateInterlocutor(data.interlocutor);
        updateOtherCountry(data.country);
      });
    });
    /* ...or denial! */
    socket.on("used username", () => {
      // status = NO_USERNAME;
      // alert("Used username");
      // document.addEventListener("keydown", enterKeyUsernameProp);
    });
  });
}

/* Enter key triggers a connection attempt */
function enterKeyConnectAttempt(e) {
  if (e.key === "Enter") connectionAttempt();
}

/* Enter key triggers a username proposal */
function enterKeyUsernameProp(e) {
  if (e.key === "Enter") setUsername(document.socket);
}

/* Enter key sends a message */
function enterKeySendsMsg(e) {
  if (e.key === "Enter") {
    sendMessage(document.socket);
  }
}

// Start by sending connection attempts
// document.addEventListener("keydown", enterKeyConnectAttempt);

/* Display connection timeout notification */
function connectionError() {
  let message =
    "<font size=\"3\"><span uk-icon='icon: warning; ratio: 1.5'></span>  Il semble que vous ayez des difficultés à vous connecter...</font>";
  UIkit.notification({
    message: message,
    pos: "bottom-center",
    status: "warning",
  });
}

/* Remove welcome page overlay */
function welcomPageOverlayOff() {
  document.getElementById("welcomePage").style.height = "0%";
}

/* Remove username form page overlay */
function usernameFormPageOverlayOff() {
  document.getElementById("usernameFormPage").style.height = "0%";
}

function spinnerPageOverlayOff() {
  // document.querySelector(".loading").style.display = "none";
}

/* Propose a username to server */
function setUsername(socket) {
  if (status === USERNAME_PENDING) return 1;
  // Get the string
  let username = getCookie("username");
  // Some basic validation
  if (username == undefined) {
    window.location.replace("/");
  } else {
    status = USERNAME_PENDING;
    socket.emit("username proposal", username);
  }
  document.querySelector(".user-you .name").innerHTML = username;
}

var displayed_messages = document.querySelector(".chat");

/* Callback on sendButton click or EnterKey event */
function sendMessage(socket) {
  // get the message input element
  let inputField = document.getElementById("message");
  // get its value
  let message = inputField.value;
  console.log("DEBUG//", message, message == "\n");
  // do nothing if message is empty
  if (message == "") {
    return false;
  }
  // otherwise send it to the room...
  socket.emit("chat message", message);
  // ...empty the input field...
  inputField.value = "";
  // ...and add it to displayed messages list
  appendNewMessage(displayed_messages, message, true, username);
}

/* Ask the server: "Hey server, how many people are connected?" */
function askHowMany(socket) {
  socket.emit("how many?");
}

/* Send a 'connect' message to server,
 * remove event listener to prevent multiple connections */
function connectionAttempt() {
  document.removeEventListener("keydown", enterKeyConnectAttempt);
  connect();
}

/* Check if user is typing */
function checkUserIsTyping(socket) {
  let searchTimeout;
  document.getElementById("message").onkeypress = (e) => {
    if (searchTimeout != undefined) clearTimeout(searchTimeout);
    searchTimeout = setTimeout(function () {
      if (e.key != "Enter") {
        socket.emit("user typing");
      }
    }, 250);
  };
}

/* Update the page to display new message, received or sent.
 * <source> is true if message is sent by user, false if received */
function appendNewMessage(displayed_messages, new_msg, source, username) {
  /* display new ones */
  // <div class="message other">What's up?</div>
  let new_message_instance = document.createElement("div");
  // sub-div

  /* // paragraph (text-container)
    let d = document.createElement('div')
    new_sub_msg_inst.appendChild(p) */
  // apply relevant style
  if (source) {
    new_message_instance.classList.add("message", "you");
  } else {
    new_message_instance.classList.add("message", "other");
  }
  /*  // put da text into da p
    // username?
    p.appendChild(document.createTextNode(new_msg)) */
  new_message_instance.appendChild(document.createTextNode(new_msg));
  displayed_messages.appendChild(new_message_instance);
  // scroll to bottom
  displayed_messages.scrollTo(0, displayed_messages.scrollHeight);
}

/* Update people counter */
function updatePeopleCounter(count) {
  console.log("People connected: " + count);
}

/* Print interlocutor */
function updateInterlocutor(interlocutor) {
  // We connected to someone
  if (interlocutor == undefined) {
    // alert("Waiting for connection");
  } else {
    document.body.classList.add("chatting");
    document.querySelector(".loading").style.display = "none";
    document.querySelector(".window").style.display = "block";
    document.querySelector(".user-other .name").innerHTML = interlocutor;
    // alert("Connected to " + interlocutor);
  }
}
/* Print interlocutor */
function updateOtherCountry(country) {
  // We connected to someone

  document
    .getElementById("flag-other")
    .setAttribute("src", "images/flags/" + country + ".svg");
}

/* Height of text input area changes with content */
function resize_msg_input_area() {
  var tx = document.getElementById("message");
  tx.addEventListener("input", onInputAdaptHeight, false);
  function onInputAdaptHeight() {
    if (this.value == "\n") this.value = "";
  }
}

/* Change interlocutor ON HOLD */
/* function changeInterlocutor(socket) {
    socket.emit('change interloc')
    console.log('asked to change interlocutor, must be boring')
} */

/* Handle received messages */
function listenToIncomingMessages(socket) {
  socket.on("chat message", (data) => {
    appendNewMessage(displayed_messages, data.message, false, data.sender);
  });

  socket.on("user typing", () => {
    // TODO: Modify it so user can see if his oponent is typing
    // display a notification
    document.querySelector(".user-other .status").innerHTML = "typing...";
    setTimeout(function () {
      document.querySelector(".user-other .status").innerHTML = "Online";
    }, 2000);
  });

  socket.on("change interloc", () => {
    //displayed_messages.innerHTML = ''
    console.log("Now talking to a brand new face!");
  });

  socket.on("greeting", (data) => {
    // display a notification
    // let notif =
    //   "<font size=\"2\"><span uk-icon='icon: user'></span> " +
    //   data.newcommer +
    //   " vient de se connecter !</font>";
    // UIkit.notification({ message: notif, pos: "top-right" });
    updatePeopleCounter(data.peoplecount);
  });
  socket.on("founduser", (data) => {
    console.log("Found user to chat to " + data);
  });

  socket.on("byebye", (data) => {
    // display a notification
    // let notif =
    //   "<font size=\"2\"><span uk-icon='icon: user'></span> " +
    //   data.leaver +
    //   " vient de partir !</font>";
    // UIkit.notification({ message: notif, pos: "top-right" });
    const count = data.peoplecount - 1;
    updatePeopleCounter(count);
  });

  socket.on("how many?", (count) => {
    updatePeopleCounter(count);
  });

  socket.on("hello", (data) => {
    console.log(data);
    document.querySelector(".loading").style.display = "none";
    document.querySelector(".window").style.display = "block";
    updateInterlocutor(data.username);
    updateOtherCountry(data.country);
  });

  socket.on("leaving", (data) => {
    console.log("Oponent left");
    window.location.replace("/new-chat");
    updateInterlocutor(data.username);
  });
}

function setFlag() {
  let country;
  country = getCookie("country");
  document
    .getElementById("flag-you")
    .setAttribute("src", "images/flags/" + country + ".svg");
}

/* COMMENTS HERE */
function initChat(socket) {
  /* Ask how many people connected */
  askHowMany(socket);
  /* Listen to incoming messages */
  listenToIncomingMessages(socket);
  /* Check if user is typing */
  checkUserIsTyping(socket);
  /* Dynamic text input area*/
  resize_msg_input_area();
  // Sets flag for user:
  setFlag();
}
