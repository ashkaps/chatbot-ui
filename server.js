/* eslint-disable indent */

/*******************************************/
/* /////////// Server creation \\\\\\\\\\\ */
/*******************************************/

const express = require("express");
const path = require("path");
// app represents the logic of the server.
const app = express();
// http is the server
const http = require("http").createServer(app);
const io = require("socket.io")(http);
const axios = require('axios');
const clientIo = require("socket.io-client");

var geoip = require("geoip-lite");

// mark 'public' as the public dir
app.use(express.static(path.join(__dirname, "public")));

// all routing logic is handled by routes/router.js
app.use("/", require("./routes/router"));

/*******************************************/
/* ///////////// pseudo-DB \\\\\\\\\\\\\\\ */
/*******************************************/

/* Fixed array of available chat rooms.
/* A socket should be part of less than one
 * of these at any given time. */
let chat_rooms = ["1", "2", "3", "4", "5", "6", "7"];
let nb_chat_rooms = chat_rooms.length; // fixed for now

/* Array of "people" object.
 * To each socket(.id) corresponds a unique human-chosen username.
 * --- Model ---
 * user = {
 *  id: socket.id,     (<- string  - unique hash given by socket.io)
 *  username: usrnm,   (<- string  - chosen by user)
 *  room: i            (<- integer - managed by server)
 * }
 * --- 'pseudo-DB' ---
 * users = [user1, user2, ...] */
let users = [];

/*
 * lucas
 * simple bot configuration
 */
let botConfigs = {
  name: 'bot',
  botChatNameDefault: "Toby",
  country: 'US',
  secret: Math.floor(Math.random() * (9999999 - 1000000) + 1000000).toString(),
  rooms: [],
  botHttpClient: axios.create({
    baseURL: 'http://161.35.182.217:5000/bot/',
    timeout: 10000
  }),
  botSocketServer: null,
  botSocketClient: null,
  botIsSetUp: false,
  botDefaultMessages: {
    greetings: ["Hi", "Hey", "Hello", "Hey there", "How's it going?"],
    onAPIError: ["Sorry I can't talk", "Sorry, I don't understand"]
  },
  startTalkOnNonResponseTimeout: 7000,
  changeToBotRoomOnNonUserAvailableTimeout: 6000,
  noConversationTimeout: 30000,
};

/*******************************************/
/* /////////////// Utils \\\\\\\\\\\\\\\\\ */
/*******************************************/
function startBotSocketClient() {
  botConfigs.botSocketClient = clientIo("ws://localhost:8080", {
    query: {
      type: 'localbot',
      botToken: botConfigs.secret
    }
  });

  botConfigs.botSocketClient.on('hello', message => {
    console.log("BOT CLIENT: received a HELLO message", message);
    const room = botGetRoomBySocketId(message.id);

    room.user = {
      id: message.id,
      name: message.username,
      country: message.country
    };

    setTimeout(()=>{
      if (room.userStartedTalk)
        return;

      const greetingsLen = botConfigs.botDefaultMessages.greetings.length;
      const greeting = botConfigs.botDefaultMessages.greetings[Math.floor(Math.random()*greetingsLen)];

      botAPIInteract(room.user.id, greeting, room.botName).then(botResponse => {

        botConfigs.botSocketServer.broadcast.to(room.name).emit("chat message", {
          message: botResponse.message,
          sender: room.botName ? room.botName : botConfigs.botChatNameDefault
        });

        if (botResponse.disconnect) {
          const OnTimeout = new Promise((resolve) => {
            setTimeout(resolve, 3000, room);
          });

          roomOnTimeout.then(roomToDisconnect => {
            return botRoomDisconnect(roomToDisconnect);
          }).then(disconnectedRoom => {
            console.log("left room ok", disconnectedRoom);
          }, fail => {
            console.log("left room fail", room);
          });

        } else {
          room.noResponseTimer = setTimeout((toDisconnect) => {

            botRoomDisconnect(toDisconnect)
              .then(disconnectedRoom => {
                console.log("left room on no conversation timeout ok", disconnectedRoom);
              }, fail => {
                console.log("left room on no conversation timeout fail", toDisconnect);
              });

          }, botConfigs.noConversationTimeout, room);
        }
      });
    }, botConfigs.startTalkOnNonResponseTimeout);

  });

  botConfigs.botSocketClient.on('byebye', message => {
    console.log("BOT CLIENT: bybye received", message);
  });

  botConfigs.botSocketClient.on("leaving", message => {
    console.log("BOT CLIENT: leaving received", message);
    const room = botGetRoomBySocketId(message.id);

    botRoomDisconnect(room).then(disconnected => {
      console.log("left room ok", disconnected);
    }, (fail) => {
      console.log("left room fail");
    });

  });

  botConfigs.botSocketClient.on('chat message', message => {
    console.log("BOT CLIENT: received a message", message);
    const room = botGetRoomByUsername(message.sender);

    if (!room.userStartedTalk)
      room.userStartedTalk = true;

    if (room.noResponseTimer) {
      clearTimeout(room.noResponseTimer);
      room.noResponseTimer = null;
    }

    botAPIInteract(room.user.id, message.message, room.botName)
      .then(botResponse => {

        botConfigs.botSocketServer.broadcast.to(room.name).emit("chat message", {
          message: botResponse.message,
          sender: room.botName ? room.botName : botConfigs.botChatNameDefault
        });

        if (botResponse.disconnect) {
          const OnTimeout = new Promise((resolve) => {
            setTimeout(resolve, 3000, room);
          });

          roomOnTimeout.then(roomToDisconnect => {
            return botRoomDisconnect(roomToDisconnect);
          }).then(disconnectedRoom => {
            console.log("left room ok", disconnectedRoom);
          }, fail => {
            console.log("left room fail", room);
          });

        } else {
          room.noResponseTimer = setTimeout((toDisconnect) => {

            botRoomDisconnect(toDisconnect)
              .then(disconnectedRoom => {
                console.log("left room on no conversation timeout ok", disconnectedRoom);
              }, fail => {
                console.log("left room on no conversation timeout fail", toDisconnect);
              });

          }, botConfigs.noConversationTimeout, room);
        }
      });
  });
}

function isTheBotClient(socket) {
  const {type, botToken} = socket.handshake.query;
  const result = type === 'localbot' && botToken === botConfigs.secret;
  return result;
}

function botServerSetUp(socket) {
  botConfigs.botSocketServer = socket;

  users.push({
    id: socket.id,
    username: botConfigs.name,
    room: undefined,
    country: botConfigs.country,
  });

  botConfigs.botSocketServer.on('chat message', (message)=>{
    console.log("BOT SERVER: message is", message);
  });

  return true;
}

/**
 * lucas
 */
function botCreateRoom(clientSocket, first_connection = true) {
  const room = {
    chatSocketId: clientSocket.id,
    name: `${clientSocket.id}-bot`
  };

  botConfigs.rooms.push(room);

  /**
   * lucas
   * Change to bot rooms and send the first message to the bot
   */
  botConfigs.botSocketServer.join(room.name, ()=>{
    console.log("bot join to room", room.name);
    changeRoom(clientSocket, room.name, first_connection, () => {
      console.log("sending hi message to bot");
    });
  });

  return room;
}

function botRoomMonitor(room, firstSocket) {
  io.in(room).clients((error, clients) => {
    console.log(`clients in room ${room} ${JSON.stringify(clients)}`);
    if (clients.length === 1) {
      console.log("adding user to a bot room");
      botCreateRoom(firstSocket, false);
    }
  });
}

function botRoomDisconnect(room) {
  return new Promise((resolve, reject) => {

    botConfigs.botSocketServer.broadcast.to(room.name).emit("leaving", {
      id: room.user.id,
      username: undefined
    });

    setTimeout(()=>{
      botConfigs.botSocketServer.leave(room.name, (err)=>{
        console.log("leaving room", room.name, err);
        const botRoomIdx = botConfigs.rooms.findIndex((botRoom) => botRoom.name === room.name);

        if (botRoomIdx === -1) {
          console.log(`Room ${room.name} not found`);
          reject(null);
        }

        const removed = botConfigs.rooms.splice(botRoomIdx, 1);

        resolve(removed);
      });
    }, 10000);

  });
}

/*
 * lucas
 */
function botGetRoomBySocketId(id) {
  return botConfigs.rooms.find(room => room.name.includes(id));
}

function botGetRoomByUsername(username) {
  return botConfigs.rooms.find(room => {
    if (room.user)
      return room.user.name === username;
    return false;
  });
}

/*
 * lucas
 *
 * send message to bot
 */
function botAPIInteract(id, message,name) {
    return botConfigs.botHttpClient.post('interact', {
        "sid": id,
        "user_message": message,
        "bot_name": name
    })
    .then((success) => {
      const failMsg = "";

      if (success.data.status != 200) {
        console.log("bot server invalid success status code", success);
        failMsg = botConfigs.botDefaultMessages.onAPIError[0];
      }

      const botResponse = {
        disconnect: success.data.disconnect,
        message: failMsg ? failMsg : success.data.bot_message
      };

      return botResponse;
    }, fail => {
      const botResponse = {
        disconnect: true,
        message: botConfigs.botDefaultMessages.onAPIError[0]
      };

      console.error(fail);

      return botResponse;
    });
}

function botAPIGetName(id) {
  return botConfigs.botHttpClient.post('name', {
    "sid": id
  });
}

// function searchForChatRoomAvoidUser(socket, user, room_index = 0) {
//     /* Recursively search for an available chat room, avoiding 'user'.
//      * If chat room of index 'room_index' in array 'chat_rooms'
//      * full, or 'user' in it, try next, if existing. */
//     let room = chat_rooms[room_index]
//     io.in(room).clients((error, clients) => {
//         if (error) console.log(error)
//         //try less than 1 member in room and 'user' not member
//         if (clients.length < 2 && !(user in clients)) {
//             changeRoom(socket, room)
//         } else if (room_index + 1 < nb_chat_rooms) {
//             //if not suitable, try next chat room, if existing
//             searchForChatRoomAvoidUser(socket, user, room_index + 1)
//         } else {
//             //else retry whole search process later
//             console.log(
//                 'no room found for socket ' +
//                 socket.id +
//                 ', searching again in 3s',
//             )
//             setTimeout(searchForChatRoom, 3000, socket)
//         }
//     })
// }

function searchForChatRoom(socket, room_index = 0, first_connection = false) {
  /* Recursively search for an available chat room.
   * If chat room of index 'room_index' in array 'chat_rooms'
   * is full, try next, if existing.
   * If none found, retry from beginning in 3s. */
  let room = chat_rooms[room_index];
  io.in(room).clients((error, clients) => {
    if (error) console.log(error);
    if (clients.length < 2) {

      changeRoom(socket, room, first_connection, () => {

        setTimeout(botRoomMonitor, botConfigs.changeToBotRoomOnNonUserAvailableTimeout, room, socket);

      });

    } else if (room_index + 1 < nb_chat_rooms) {
      searchForChatRoom(socket, room_index + 1, first_connection);
    } else {
      const botRoom = botCreateRoom(socket);

      console.log(
        "no available room found for socket " +
          socket.id +
          ", adding to bot room" + botRoom.name
      );

      //setTimeout(searchForChatRoom, 3000, socket, 0, first_connection);
    }
  });
}

function changeRoom(socket, room, first_connection = false, callback) {
  /* Make socket leave its current chat room (if existing,
   * and if not first connection), and join 'room'
   * (to ensure uniqueness). */
  // Get user's current chat room
  findEmittingRoom(
    socket,
    (socket_room) => {
      // If found, and if not first connection...
      if (socket_room != undefined && !first_connection) {
        // ...leave it...
        socket.leave(socket_room, () => {
          console.log("socket " + socket.id + " left room " + room);
          io.to(socket.id).emit("left room", socket_room);
          // ...then join the new one
          joinRoom(socket, room, first_connection, callback);
        });
      } else joinRoom(socket, room, first_connection, callback);
    },
    first_connection
  );
}

function joinRoom(socket, room, first_connection, callback) {
  /* Simply join the given room, without any checks.
   * Then, get/emit details from/to that new room, if it isn't 'general',
   * and update the pseudo-DB.
   * The callback does not wait for details. */
  socket.join(room, () => {
    io.in(room).clients((error, clients) => {
      console.log("Clients in room " + room + ": " + clients);
    });
    /* Update DB */
    changeRoomOfUserInDB(socket, room, () => {
      /* Get/Emit details, and/or callback */
      if (room != "general") {
        // Get room details
        emitNewRoomDetailsToSocket(socket, first_connection, room);
        // Broadcast details of socket to new room
        broadcastSocketDetailsToNewRoom(socket, room);
      }
    });
    callback();
  });
}

function changeRoomOfUserInDB(socket, room, callback) {
  /* Update the pseudo-DB changing 'room' attribute
   * to socket. */
  getUserFromSocket(socket, (user) => {
    if (user === undefined) {
      console.log("Error: user is undefined in changeRoomToUserObjInDB!");
    } else {
      user.room = room;
    }
    callback();
  });
}

function getUserFromSocket(socket, callback) {
  /* Return to callback the (first) 'user' element
   * of list 'users' which id corresponds to that of socket. */
  let user = users.find((usr) => usr.id === socket.id);
  callback(user);
}

function broadcastSocketDetailsToNewRoom(socket, new_room) {
  /* Broadcast sockets' det... ok just read the title. */
  let usrnm = findUsername(socket.id);
  let cntry = findCountry(socket.id);
  socket.broadcast.to(new_room).emit("hello", {
    id: socket.id,
    username: usrnm,
    country: cntry,
  });
}

function emitNewRoomDetailsToSocket(socket, first_connection, new_room) {
  /* Emit new room details to joining socket. */
  getInterloc(socket, first_connection, (interloc) => {
    console.log(interloc);
    getUsernamefromgetInterlocReturn(interloc, (usrnm, cntry) => {
      io.to(socket.id).emit("joined room", {
        room: new_room,
        interlocutor: usrnm,
        country: cntry,
      });
    });
  });
}

function getInterloc(socket, first_connection, callback) {
  /* Get the interlocutor' socket id and username of a given user (socket).
   * Assumes there are no more than 2 people by chat room. */
  findEmittingRoom(
    socket,
    (emitting_room) => {
      if (emitting_room != undefined) {
        let interloc_id;
        io.in(emitting_room).clients(async (error, clients) => {
          if (error) console.log(error);
          for (let client of clients) {
            if (client != socket.id) {
              console.log(client);
              interloc_id = client;
              let usrnm;
              if (interloc_id != undefined) {
                usrnm = findUsername(interloc_id);
                cntry = findCountry(interloc_id);

                if (usrnm === botConfigs.name) {
                  let newName = "";

                  try {
                    const response = await botAPIGetName(socket.id);
                    newName = response.data.bot_name ? response.data.bot_name : botConfigs.botChatNameDefault;
                  } catch (err) {
                    console.error(err);
                    newName = botConfigs.botChatNameDefault;
                  } finally {
                    const room = botGetRoomBySocketId(socket.id);

                    console.log("bot name", newName);
                    room.botName = newName;
                    usrnm = newName;
                    cntry = findCountry(socket.id);
                  }

                }

              } else usrnm = undefined;
              var interloc = {
                id: interloc_id,
                username: usrnm,
                country: cntry,
              };
              return callback(interloc);
            } // COMMENTS/SEPARATE CODE PLZ
          }
          return callback(undefined);
        });
      } else {
        if (!first_connection)
          console.log(
            "Error: couldn't find emitting room for socket " +
              socket.id +
              " in getInterloc!"
          );
        return callback(undefined);
      }
    },
    first_connection
  );
}

function getUsernamefromgetInterlocReturn(interloc, callback) {
  /* Callback on getInterloc return, becauz async's hard bruh */
  let usrnm = undefined;
  let cntry = undefined;
  if (interloc !== undefined) {
    usrnm = interloc.username;
    cntry = interloc.country;
  }
  callback(usrnm, cntry);
}

function findEmittingRoom(socket, callback, first_connection = false) {
  /* Return the emitting room the socket is in.
   * Assumes that the socket is only part of 'general',
   * one chat room or less, and its own ('socket.id').
   * Will log to console if not found, unless
   * first connection specified  */
  let rooms = Object.keys(socket.rooms);
  let emitting_room = undefined;
  for (let room of rooms) {
    if (room != socket.id && room != "general") {
      emitting_room = room;
      break;
    } // .filter ?
  }
  if (emitting_room === undefined && !first_connection) {
    console.log(
      "Error: couldn't find emitting room for socket " +
        socket.id +
        " in findEmittingRoom!"
    );
  } else emitting_room;
  callback(emitting_room);
}

function findUsername(socket_id) {
  /* Get the username of a user's id.
   * Assumes the socket has a valid (ie existing and unique)
   * username in "users" list.
   * Returns undefined if socket_id is undefined. */
  if (socket_id === undefined) {
    console.log("Error: socket is undefined in findUsername!");
    return undefined;
  } else
    for (let i = 0; i < users.length; i++) {
      if (users[i].id == socket_id) {
        return users[i].username;
      }
    }
}
function findCountry(socket_id) {
  /* Get the username of a user's id.
   * Assumes the socket has a valid (ie existing and unique)
   * username in "users" list.
   * Returns undefined if socket_id is undefined. */
  if (socket_id === undefined) {
    console.log("Error: socket is undefined in findUsername!");
    return undefined;
  } else
    for (let i = 0; i < users.length; i++) {
      if (users[i].id == socket_id) {
        return users[i].country;
      }
    }
}

function disconnectionHandler(socket) {
  /* Handle the whole(some) response to a 'disconnet' event */
  createLeavingMessageInfo(socket, (room, usrnm) => {
    sendLeavingMessage(socket, room);
    updatePeopleCounters(usrnm);
    removeUserOfDB(socket);
  });
}

function createLeavingMessageInfo(socket, callback) {
  /* Build the message sent to the former room of the leaver */
  getUserFromSocket(socket, (user) => {
    if (user === undefined) {
      callback(undefined, undefined);
      console.log(
        "Error: could not find user while creating leaving message for socket " +
          socket.id +
          "!"
      );
    } else callback(user.room, user.username);
  });
}

function sendLeavingMessage(socket, room) {
  /* Send a message to the former room of the leaver */
  if (room === undefined) {
    console.log(
      "Error: no leaving message sent to (unknown) former room of socket " +
        socket.id +
        "!"
    );
  } else {
    socket.broadcast.to(room).emit("leaving", {
      id: socket.id,
      username: undefined, // beacause he/she just left!
    });
  }
}

function removeUserOfDB(socket) {
  /* Remove the (first) user corresponding to socket's id
   * from "users" list (assuming only one user corresponding to id) */
  for (let i = 0; i < users.length; i++) {
    if (users[i].id == socket.id) {
      users.splice(i, 1);
      break;
    }
  }
}

function updatePeopleCounters(usrnm) {
  /* Tell everybody somebody just left, and update the peoplecounters */
  io.in("general").clients((error, clients) => {
    if (error) console.log(error);
    io.to("general").emit("byebye", {
      leaver: usrnm,
      peoplecount: clients.length,
    });
  });
}

/*******************************************/
/* ///////////// Entry point \\\\\\\\\\\\\ */
/*******************************************/

/* On connection of socket... */
io.on("connect", (socket) => {

  if (isTheBotClient(socket)) {
    botConfigs.botIsSetUp = botServerSetUp(socket);
    return;
  }

  /* Add user to pseudo-DB */
  var geoip = require("geoip-lite");

  var ip =
    socket.handshake.headers["x-forwarded-for"] != undefined
      ? socket.handshake.headers["x-forwarded-for"].split(",")[0]
      : "178.165.22.132";
  var geo = geoip.lookup(ip);

  users.push({
    id: socket.id,
    username: undefined,
    room: undefined,
    country: geo != null ? geo.country : "US",
  });

  console.log("-> socket " + socket.id + " just connected ->");

  /* Send connection confirmation */
  io.to(socket.id).emit("connection success");

  /* Everybody joins 'general' on first connection */
  changeRoom(socket, "general", true, () => {
    // Then, tell everybody else somebody just connected,
    // and upgrade the peoplecounter

    // io.emit("connect","34343")
    io.in("general").clients((error, clients) => {
      if (error) console.log(error);
      socket.broadcast.to("general").emit("greeting", {
        newcommer: socket.id,
        peoplecount: clients.length,
      });
    });

  });

  /* Check username proposals */
  socket.on("username proposal", (username) => {
    if (users.some((user) => user.username == username)) {
      // reject
      io.to(socket.id).emit("used username");
    } else {
      // accept
      getUserFromSocket(socket, (user) => {
        if (user === undefined) {
          console.log(
            "Error: couldn't find user with id " + socket.id + " in DB!"
          );
          io.to(socket.id).emit("error finding user in DB");
        } else {
          // write to DB, confirm username to socket
          user.username = username;
          io.to(socket.id).emit("accepted username", username);

          /***********************/
          /* Initiate chat logic */
          /***********************/

          /* Check for an available chat room, loop until found */
          searchForChatRoom(socket, 0, true);
        }
      });
    }
  });

  /* Respond to "people count" requests */
  socket.on("how many?", () => {
    io.in("general").clients((error, clients) => {
      if (error) console.log(error);
      io.to(socket.id).emit("how many?", clients.length);
    });
  });

  /* Transmit chat messages to adequate rooms */
  socket.on("chat message", (msg) => {
    // Find the room it originates from (NOT undefined-proof)
    findEmittingRoom(socket, (emitting_room) => {
      // get the username of the sender (undefined-proof)
      let username = findUsername(socket.id);
      if (emitting_room != undefined) {
        // Send the message back to the room, but not to the sender
        socket.broadcast.to(emitting_room).emit("chat message", {
          message: msg,
          sender: username,
        });
      } else {
        console.log(
          "Error: could not find emitting room while chat message received!"
        );
        io.to(socket.id).emit("message sending error");
      }
    });
  });

  /* Send typing indicator message */
  socket.on("user typing", () => {
    //find the room it originates from
    findEmittingRoom(socket, (emitting_room) => {
      if (emitting_room != undefined) {
        //send the message back to the room, but not to the sender
        socket.broadcast.to(emitting_room).emit("user typing");
      } else {
        console.log(
          "Error: could not find emitting room while user typing message received!"
        );
      }
    });
  });

  /* Change of interlocutor ON HOLD */
  /* socket.on('change interloc', () => {
        console.log('User ' + socket.id + ' asked to change interlocutor')
        let interloc = (socket)
        searchForChatRoomAvoidUser(socket, interloc.id)
    }) */

  /* Handle disconnection */
  socket.on("disconnect", (reason) => {
    console.log(
      "<- socket " + socket.id + " just left ; cause: " + reason + " <-"
    );
    disconnectionHandler(socket);
  });
});

/********************************************/
/* /////////// bot socket client \\\\\\\\\\\*/
/********************************************/
startBotSocketClient();

/*******************************************/
/* //////////// server start \\\\\\\\\\\\\ */
/*******************************************/

// Heroku configuration
let port = process.env.PORT;
if (port == null || port == "") port = 8080;

http.listen(port, () => {
  const DASHESNL = "-----------------------\n";
  console.log(DASHESNL + "Listening on port 8080\n" + DASHESNL);
});

module.exports = app;
